package com.intertrust;

public class App {
    public static void main(String[] args) {
        TicTacToe game = new TicTacToe();
        game.play("X", 1, 1);
        game.play("O", 0, 0);
        game.play("X", 0, 1);
        game.play("O", 1, 0);
        game.play("X", 2, 1);
        System.out.println("Winner is " + game.winner());
    }
}
